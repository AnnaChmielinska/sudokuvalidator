package pl.datumo.sudoku.validator.domain

object BoardValidator{

  private final val startPoint: Int = 0;

  def apply(boardSize: Int, emptyFieldNumericalValue: Int, startPoint: Int= startPoint): BoardValidator = {
    validateBoardSizeIsSquareNumber(boardSize)
    new BoardValidator(boardSize, startPoint)
  }

  private[validator] def validateBoardSizeIsSquareNumber(boardSize: Int): Unit = {
    val sqrt = math.sqrt(boardSize)
    if (sqrt % 1 != 0)
      throw new BoardValidationException(s"Board Size: $boardSize is not a perfect square.")
  }
}



class BoardValidator(boardSize: Int, emptyFieldNumericalValue: Int, startPoint: Int = BoardValidator.startPoint) {

  @throws(classOf[BoardValidationException])
  def validate( in: Array[Array[Int]]): Unit = {
    validateRows( in )
    validateColumns( in )
    validateSubtables( in )

  }

 @throws(classOf[BoardValidationException])
 private[validator] def validateRows( in: Array[Array[Int]]): Unit = {
    for( i <- startPoint until boardSize) {
      if( in(i).length != boardSize ) throw new BoardValidationException(s"Row : $i has wrong size.")
      if( !hasValues1toN( in(i) ) ) throw new BoardValidationException(s"Row : $i contains incorrect data.")
    }


 }

  @throws(classOf[BoardValidationException])
  private[validator] def validateColumns(in: Array[Array[Int]]): Unit = {

    val transposedArray = in.transpose

    for( i <- startPoint until boardSize) {
      if( transposedArray(i).length != boardSize ) throw new BoardValidationException(s"Column : $i has wrong size.")
      if (!hasValues1toN(transposedArray(i))) throw new BoardValidationException(s"Column : $i contains incorrect data.")
    }

  }


  @throws(classOf[BoardValidationException])
  private[validator] def validateSubtables(in: Array[Array[Int]]): Unit = {

    val sqrtBoardSize = math.sqrt(boardSize).toInt
    val squaresAsRows = in.grouped(sqrtBoardSize).toArray.map(_.transpose).map(_.grouped(sqrtBoardSize).toArray).flatMap(_.map(_.flatten))

    for( i <- 0 until boardSize) if( !hasValues1toN( squaresAsRows(i) ) )
      throw new BoardValidationException(s"SubTable : $i contains incorrect data.")


  }


  private[validator] def hasValues1toN(row: Array[Int]) = row.distinct.count(1 to boardSize contains _) == row.count(_ != emptyFieldNumericalValue)
}
