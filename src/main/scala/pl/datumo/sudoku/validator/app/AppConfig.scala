package pl.datumo.sudoku.validator.app

import com.typesafe.config.{Config, ConfigFactory}

case class AppConfig( tableSize: Int, emptyFieldNumericalValue: Int, inputFileSeparator: String) extends Serializable

object AppConfig {

  private final val rootConfigName = "sudokuValidator"

  def apply(configRoot: String = rootConfigName): AppConfig = {

    val config = ConfigFactory.load(configRoot).withFallback(ConfigFactory.load())
    create(config)
  }

  private def create(config: Config): AppConfig = {
    import net.ceedubs.ficus.Ficus._

    AppConfig( config.getInt(s"$rootConfigName.tableSize"), config.getInt(s"$rootConfigName.emptyFieldNumericalValue"), config.getString(s"$rootConfigName.inputFileSeparator") )
  }


}