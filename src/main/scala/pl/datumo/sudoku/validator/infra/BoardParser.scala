package pl.datumo.sudoku.validator.infra

import java.io.IOException


object BoardParser {



  @throws(classOf[IOException])
  @throws( classOf[BoardParserException] )
  def readFile(fileName: String, separator: String ) : Array[Array[Int]] = {

    val bufferedSource = io.Source.fromFile( fileName )
    val board = bufferedSource.getLines().map(_.split( separator ).map(toInt(_))).toArray

    bufferedSource.close()

    board
  }



  @throws( classOf[BoardParserException] )
  private[validator] def toInt(in: String):Int = {
    val trimmed = in.trim
    if (trimmed.isEmpty) 0
    else try{trimmed.toInt}
         catch { case e: NumberFormatException => throw new BoardParserException(s"Incorrect value $trimmed is not a number in input file.") }
  }





}
